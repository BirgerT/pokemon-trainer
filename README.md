# Pokemon Trainer

Javascript assignment for the Noroff .NET upskill course winter 2021:
Build a Pokemon Trainer web app using the Angular Framework

## Setup
<ul>
<li> Clone the repository </li>
<li> npm install </li>
<li> npm run serve </li>
<li> ng serve </li>
<li> Open the path the app is running at. (Typically http://localhost:4200/) </li>
</ul>

## Authors
<ul>
<li> Birger Topphol </li>
<li> Elise Rishaug </li>
</ul>

## Features
Pokemon trainer web application let you catch pokemons. Take use of the Pokemon API to display Pokemon with their avatars: https://pokeapi.co/. Uses Json server to store trainer and their caught pokemons. 

### Landing page: 
Enter application by sign in with your trainer name. The Trainer name is stored in the database solution and take use of local storage. 

### Pokemon Catalouge:
Users do not have access to this page unless they have entered a trainer name.
The Pokemon catalogue is a list of “cards” Pokemon presented to the user. The
PokemonCard have an image (sprite) and name displayed. Each Pokémon card is clickable and take the user to the Pokemon detail page.

### Pokémon detail: 
Display's an image of the Pokemon along with its abilities, stats, height, weight and types. There is a button to “Catch” this Pokémon for the logged in Trainer. This is then stored locally and used to display the collected Pokemon in the Trainer page. 

### Trainer page: 
The trainer page display's all the Pokemon the Trainer has catched. It display cardcomponent of caught pokemon. Each pokemon in the list is clickable and take the user to the Pokemon Detail page for the specific Pokemon that was clicked. Log out button will end this user session redirect to "landing" page.

## Assignment requirements

<p>&#10004; Use the latest Angular with the Angular CLI</p>
<p>&#10004; Use Components to: Create “Root” or “Parent” components for pages, reusable pieces of UI </p>
<p>&#10004; MUST Use the Angular Router</p>
<p>&#10004; Use the Angular Guard pattern</p>
<p>&#10004; Use Services to: Share data between components </p>
<p>&#10004; Use Services to: Make HTTP Requests using the HttpClient </p>
<p>&#10004; Group your components logically </p>
<p>&#10004; Use Guards to stop users from accessing pages that require a login </p>
