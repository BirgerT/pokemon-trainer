import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Pages
import { LandingPage } from './pages/landing/landing.page';
import { TrainerPage } from './pages/trainer/trainer.page';
import { CataloguePage } from './pages/catalogue/catalogue.page';
import { PokemonPage } from './pages/pokemon/pokemon.page';
import { NotFoundPage } from './pages/not-found/not-found.page';

// Utilities
import { AppRoutes } from './constants/app-routes.enum';
import { LoginGuard } from './guard/login.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: AppRoutes.TRAINER,
  },
  {
    path: AppRoutes.LANDING,
    component: LandingPage,
  },
  {
    path: AppRoutes.TRAINER,
    component: TrainerPage,
    canActivate: [LoginGuard],
  },
  {
    path: AppRoutes.CATALOGUE,
    component: CataloguePage,
    canActivate: [LoginGuard],
  },
  {
    path: AppRoutes.POKEMON + ':name',
    component: PokemonPage,
    canActivate: [LoginGuard],
  },
  {
    path: '**',
    component: NotFoundPage,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
