import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Pages
import { LandingPage } from './pages/landing/landing.page';
import { TrainerPage } from './pages/trainer/trainer.page';
import { CataloguePage } from './pages/catalogue/catalogue.page';
import { PokemonPage } from './pages/pokemon/pokemon.page';
import { NotFoundPage } from './pages/not-found/not-found.page';

// Components
import { HeaderComponent } from './components/header/header.component';
import { ButtonComponent } from './components/button/button.component';
import { CardComponent } from './components/card/card.component';
import { PokemonCardComponent } from './components/pokemon-card/pokemon-card.component';
import { PokemonDetailsComponent } from './components/pokemon-details/pokemon-details.component';
import { DetailCardComponent } from './components/pokemon-details/detail-card/detail-card.component';
import { TypeListComponent } from './components/pokemon-details/type-list/type-list.component';
import { StatsListComponent } from './components/pokemon-details/stats-list/stats-list.component';
import { AbilityListComponent } from './components/pokemon-details/ability-list/ability-list.component';
import { MovesListComponent } from './components/pokemon-details/moves-list/moves-list.component';
import { ImgRandomComponent } from './components/img-random-pokemon/img-random-pokemon.component';

@NgModule({
  declarations: [
    AppComponent,

    // Pages
    LandingPage,
    TrainerPage,
    CataloguePage,
    PokemonPage,
    NotFoundPage,

    // Components
    HeaderComponent,
    ButtonComponent,
    CardComponent,
    PokemonCardComponent,
    PokemonDetailsComponent,
    DetailCardComponent,
    TypeListComponent,
    StatsListComponent,
    AbilityListComponent,
    MovesListComponent,
    ImgRandomComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
