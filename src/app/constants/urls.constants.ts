export const USER_API = 'http://localhost:3000';
export const POKEMON_API = 'https://pokeapi.co/api/v2/pokemon/';
export const IMAGE_URL = (id) =>
  `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`;
