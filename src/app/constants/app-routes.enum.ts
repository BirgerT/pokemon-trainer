export enum AppRoutes {
  LANDING = 'landing',
  TRAINER = 'trainer',
  CATALOGUE = 'catalogue',
  POKEMON = 'pokemon/',
}
