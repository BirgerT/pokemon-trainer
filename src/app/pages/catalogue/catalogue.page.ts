import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { CatalougeService } from '../../services/catalouge.service';
import { Pokemon } from '../../model/pokemon.model';

@Component({
  selector: 'app-catalouge-page',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.sass'],
})
export class CataloguePage implements OnInit {
  constructor(private readonly catalougeService: CatalougeService) {}

  ngOnInit(): void {
    this.catalougeService.fetchPokemons();
  }

  get pokemons$(): Observable<Pokemon[]> {
    return this.catalougeService.pokemons$();
  }
}
