import { Component, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.sass'],
})
export class LandingPage {
  @Output() success: EventEmitter<void> = new EventEmitter();
  @Input() username: any;

  constructor(public userService: UserService, private router: Router) {}

  onLoginClick() {
    this.userService.login(this.username).subscribe(() => {
      this.success.emit();
      this.router.navigateByUrl('/trainer');
    });
  }
}
