import { Component } from '@angular/core';
import { AppRoutes } from 'src/app/constants/app-routes.enum';

// Storage
import { SessionService } from 'src/app/services/session.service';
import { getStorage } from 'src/utils/storage.utils';
import { storagekeys } from '../../constants/storagekeys.enum';

// Models
import { User } from 'src/app/model/user.model';
import { CaughtPokemon } from 'src/app/model/caught-pokemon.model';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.sass'],
})
export class TrainerPage {
  caughtPokemon: CaughtPokemon[];
  appRoutes = AppRoutes;
  userIsLoggedIn = true;

  constructor(private readonly sessionService: SessionService) {
    this.caughtPokemon = (getStorage(
      storagekeys.USER
    ) as User).pokemon.reverse();
  }

  logOut(): void {
    this.sessionService.deActive();
    this.userIsLoggedIn = false;
  }
}
