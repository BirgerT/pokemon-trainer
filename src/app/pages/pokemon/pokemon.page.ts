import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { PokemonService } from 'src/app/services/pokemon.service';
import { Pokemon } from '../../model/pokemon.model';

@Component({
  selector: 'app-pokemon-page',
  templateUrl: './pokemon.page.html',
})
export class PokemonPage implements OnInit {
  private name: string;

  constructor(
    private readonly pokemonService: PokemonService,
    route: ActivatedRoute
  ) {
    this.name = route.snapshot.paramMap.get('name');
  }

  ngOnInit(): void {
    this.pokemonService.fetchPokemon(this.name);
  }

  get pokemon$(): Observable<Pokemon> {
    return this.pokemonService.pokemon$();
  }
}
