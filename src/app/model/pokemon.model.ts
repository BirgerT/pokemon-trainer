export interface Pokemon {
  name: string;
  imageUrl?: string;
  types?: any[];
  baseStats?: any[];
  height?: number;
  weight?: number;
  abilities?: string[];
  baseExperience?: number;
  moves?: string[];
}
