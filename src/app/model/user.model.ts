import { CaughtPokemon } from './caught-pokemon.model';

export interface User {
  id: number;
  name: string;
  pokemon: CaughtPokemon[];
}
