export interface CaughtPokemon {
  name: string;
  imageUrl: string;
}
