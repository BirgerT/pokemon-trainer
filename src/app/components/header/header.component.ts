import { Component } from '@angular/core';
import { AppRoutes } from 'src/app/constants/app-routes.enum';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass'],
})
export class HeaderComponent {
  appRoutes = AppRoutes;
}
