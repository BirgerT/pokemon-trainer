import { Component } from '@angular/core';
import { AppRoutes } from 'src/app/constants/app-routes.enum';
import { IMAGE_URL } from 'src/app/constants/urls.constants';

@Component({
  selector: 'app-img-random',
  templateUrl: './img-random-pokemon.component.html',
})
export class ImgRandomComponent {
  randomPokemonId = Math.floor(Math.random() * 898);
  imageUrl = IMAGE_URL(this.randomPokemonId);
  appRoutes = AppRoutes;
}
