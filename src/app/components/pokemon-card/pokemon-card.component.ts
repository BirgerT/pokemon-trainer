import { Component, Input } from '@angular/core';
import { AppRoutes } from 'src/app/constants/app-routes.enum';
import { Pokemon } from 'src/app/model/pokemon.model';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.sass'],
})
export class PokemonCardComponent {
  @Input() pokemon: Pokemon;
  appRoutes = AppRoutes;
}
