import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-type-list',
  templateUrl: './type-list.component.html',
  styleUrls: ['./type-list.component.sass'],
})
export class TypeListComponent {
  @Input() types: string[];
}
