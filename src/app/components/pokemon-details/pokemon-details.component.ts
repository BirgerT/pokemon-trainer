import { Component, Input } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Pokemon } from 'src/app/model/pokemon.model';

@Component({
  selector: 'app-pokemon-details',
  templateUrl: './pokemon-details.component.html',
  styleUrls: ['./pokemon-details.component.sass'],
})
export class PokemonDetailsComponent {
  @Input() pokemon: Pokemon;

  constructor(private readonly userService: UserService) {}

  onCatchClicked() {
    this.userService.storePokemon(this.pokemon);
  }
}
