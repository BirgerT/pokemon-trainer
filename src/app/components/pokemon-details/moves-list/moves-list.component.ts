import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-moves-list',
  templateUrl: './moves-list.component.html',
  styleUrls: ['./moves-list.component.sass'],
})
export class MovesListComponent {
  @Input() moves: string[];
}
