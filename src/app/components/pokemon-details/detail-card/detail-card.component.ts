import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-detail-card',
  templateUrl: './detail-card.component.html',
  styleUrls: ['./detail-card.component.sass'],
})
export class DetailCardComponent {
  @Input() header: string;
}
