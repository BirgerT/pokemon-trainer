import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-stats-list',
  templateUrl: './stats-list.component.html',
  styleUrls: ['./stats-list.component.sass'],
})
export class StatsListComponent {
  @Input() stats: { name: string; baseValue: string };
}
