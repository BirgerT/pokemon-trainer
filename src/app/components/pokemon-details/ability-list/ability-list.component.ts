import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-ability-list',
  templateUrl: './ability-list.component.html',
  styleUrls: ['./ability-list.component.sass'],
})
export class AbilityListComponent {
  @Input() abilities: string[];
}
