import { Injectable } from '@angular/core';
import { getStorage, removeStorage } from 'src/utils/storage.utils';
import { storagekeys } from '../constants/storagekeys.enum';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  active(): boolean {
    const user = getStorage(storagekeys.USER);
    return Boolean(user);
  }

  deActive(): void {
    removeStorage(storagekeys.USER);
  }
}
