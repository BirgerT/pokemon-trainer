import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Pokemon } from '../model/pokemon.model';
import { ApiResponse } from '../model/api-response.model';
import { IMAGE_URL, POKEMON_API } from '../constants/urls.constants';

@Injectable({
  providedIn: 'root',
})
export class CatalougeService {
  private readonly _pokemons$: BehaviorSubject<Pokemon[]> = new BehaviorSubject(
    []
  );

  constructor(private readonly http: HttpClient) {}

  public pokemons$(): Observable<Pokemon[]> {
    return this._pokemons$.asObservable();
  }

  public fetchPokemons(): void {
    this.http
      .get<ApiResponse>(POKEMON_API)
      .pipe(map((response: ApiResponse) => response.results))
      .subscribe(
        (pokemons) => {
          pokemons.map((pokemon) => {
            const id = pokemon.url.split('/').filter(Boolean).pop();
            pokemon.imageUrl = IMAGE_URL(id);
          });
          this._pokemons$.next(pokemons);
        },
        (error) => {
          console.log(error);
        }
      );
  }
}
