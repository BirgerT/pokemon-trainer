import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { mergeMap, tap } from 'rxjs/operators';

// Storage
import { getStorage, setStorage } from 'src/utils/storage.utils';
import { storagekeys } from '../constants/storagekeys.enum';
import { USER_API } from '../constants/urls.constants';

// Models
import { User } from '../model/user.model';
import { Pokemon } from '../model/pokemon.model';
import { CaughtPokemon } from '../model/caught-pokemon.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  setUser(user: User): void {
    setStorage(storagekeys.USER, user);
  }

  login(username: string): Observable<User> {
    const login$ = this.http.get(`${USER_API}/users/?name=${username}`);
    const register$ = this.http.post(USER_API + '/users', {
      name: username,
      pokemon: [],
    });
    return login$.pipe(
      mergeMap((loginResponse: any[]) => {
        const user = loginResponse.pop();

        if (!user) {
          return register$;
        } else {
          return of(user);
        }
      }),
      tap((user) => {
        this.setUser(user);
      })
    );
  }

  storePokemon(pokemon: Pokemon): void {
    const user = getStorage(storagekeys.USER) as User;
    const caughtPokemon = this.copyCaughtPokemon(pokemon);

    if (!this.checkIfExists(caughtPokemon, user.pokemon)) {
      user.pokemon.push(caughtPokemon);
      setStorage(storagekeys.USER, user);

      this.http
        .patch(USER_API + `/users/${user.id}`, { pokemon: user.pokemon })
        .subscribe();
    }
  }

  /*
   *  Helper functions
   */
  private checkIfExists(
    pokemon: CaughtPokemon,
    list: CaughtPokemon[]
  ): boolean {
    return list.some((poke) => {
      return pokemon.name === poke.name;
    });
  }

  private copyCaughtPokemon(pokemon: Pokemon): CaughtPokemon {
    return (({ imageUrl, name }) => ({ imageUrl, name }))(pokemon);
  }
}
