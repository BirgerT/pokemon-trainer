import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

import { Pokemon } from '../model/pokemon.model';
import { POKEMON_API } from '../constants/urls.constants';

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  private readonly _pokemon$: BehaviorSubject<Pokemon> = new BehaviorSubject({
    name: '',
  });

  constructor(private readonly http: HttpClient) {}

  pokemon$(): Observable<Pokemon> {
    return this._pokemon$.asObservable();
  }

  fetchPokemon(name: string): void {
    this._pokemon$.next({ name: '' });
    this.http.get<any>(POKEMON_API + name).subscribe(
      (response: Pokemon) => {
        this._pokemon$.next(this.mapToPokemon(response));
      },
      (error) => {
        console.log('PokemonService.fetchPokemon: ' + error);
      }
    );
  }

  private mapToPokemon(response): Pokemon {
    const pokemon = {} as Pokemon;

    pokemon.name = response.name;
    pokemon.imageUrl = response.sprites.front_default;
    pokemon.types = response.types.map((type) => type.type.name);

    pokemon.height = response.height;
    pokemon.weight = response.weight;

    pokemon.baseExperience = response.base_experience;
    pokemon.baseStats = response.stats.map((stat) => ({
      name: stat.stat.name,
      baseValue: stat.base_stat,
    }));
    pokemon.abilities = response.abilities.map(
      (ability) => ability.ability.name
    );
    pokemon.moves = response.moves.map((move) => move.move.name);

    return pokemon;
  }
}
